

exports.BucketsList = function (req, res, next) {
  const AWS = require("aws-sdk");
  const fs = require("fs");
  AWS.config.update({
    accessKeyId: req.body.access_key,
    secretAccessKey: req.body.secret_key,
    region: req.body.region,
  });
  const keys = req.body;
  const region = req.body.region;
  s3 = new AWS.S3({ apiVersion: "2006-03-01" });
  let elementName = [];
  // Create the parameters for calling createBucket
  s3.listBuckets(function (err, data) {
    if (err) {
      console.log("Error not getiin list error", err.code);
      res.status(500).render('list', {data: null, aKeys:null, bucket:false, err:err});
      return;

    } else {
      for (var i = 0; i < data.Buckets.length; i++) {
        let options = data.Buckets[i];
        elementName.push(options);
      }
    }
    res.render("list", { data: elementName, aKeys:keys,region: region, bucket:true, title: "Bucket Created Succesfully!" });
  });
};


exports.uploadSourceCode = async function (req, res, next) {
  const fs = require("fs");
  const AWS = require("aws-sdk");
  s3 = new AWS.S3({ apiVersion: "2006-03-01" });


  let formBody = req.body;
  console.group("============================================================");
  console.log("Body => ",req.body)
  s3 = new AWS.S3({ apiVersion: "2006-03-01" });

  let {bucket_name, files} = req.body
  let urls = []
  files.forEach(({key, contentType}) => {
    let fileKey = key.split('/').slice(1).join('/')
    console.log(key, "=============key =======", fileKey);

      const presignedPUTURL = s3.getSignedUrl('putObject', {
        Bucket: bucket_name,
        Key: key , //filename
        Expires: 500, //time to expire in seconds
        ACL: 'public-read',
        ContentType: contentType,}
      )
      urls.push({url:presignedPUTURL, contentType:contentType});
  })


  console.log("Urls => ", urls)
  console.groupEnd()
  return res.json({
    urls
  });


};

exports.initListPage = function (req, res, next) {
  res.render("list", { data: this.elementName,bucket:false,title: "Get the list "})
}