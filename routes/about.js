const express = require("express");
const router = express.Router();
const multer = require('multer')
const fs = require('fs');
const controllers = require("../controllers/aboutController");
const BlogControllers = require("../controllers/blogsController");
const createControllers = require("../controllers/createBucket")

const loginControllers = require("../controllers/loginController");
const uploadControllers = require("../controllers/uploadSource");
const listControllers = require("../controllers/bucketListController");
const blogList = require("./blog.json");


// upload controllers

const storage = multer.memoryStorage({
    destination: function (req, file, callback) {
        callback(null, '')
    }
})

// about requests
router.get("/about-us", controllers.aboutUS);
router.post("/about", controllers.SendEmail);
// blog requests
router.get("/blogs", BlogControllers.GetBlogs);
router.get("/blogslist", BlogControllers.GetBlogsList);
router.get("/blogs/:id", BlogControllers.GetBlogByID);

// router.post("/createBlog", BlogControllers.GetBlogPostContent);
router.get("/createBlog", async function (req, res, next) {
    if(req.session.loggedin){
        res.render("blogs/createBlog");
        // res.send("logged in")
    }else {
        res.render("blogs/login")
    }
});

// router.post("/get-signed-url-blog", BlogControllers.getSignedUrlBlog)
// router.post("/upoadBlog", multer({ storage }).single('card_image'), async (req, res,next) => {
router.post("/upoadBlog", async (req, res, next) => {
    console.log(req.body.title, "blog data");
    console.log(req.body.short_description, "blog data");
    console.log(req.body.description, "blog data");
    console.log(req.body.content, "blog data");
    console.log(req.files.card_image, "file data");
    console.log(req.files.card_image.name, "name data");
    console.log(req.files.card_image.data, "data data");
    const title = req.body.title;
    const short_desription = req.body.short_desription;
    const description = req.body.description;
    const content = req.body.content;

    const key = req.files.card_image.name;
    const data = req.files.card_image.data;
    const mimetype = req.files.card_image.mimetype;

    const fs = require("fs");
    const AWS = require("aws-sdk");
    s3 = new AWS.S3({ apiVersion: "2006-03-01" });
    AWS.config.update({
        accessKeyId: "AKIAWLQYOI76JROQZQCU",
        secretAccessKey: "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",
        region: "us-east-1",
    })
    const uploadParams = {
        Bucket: "stridefutrueaws",
        Key: key,
        Body: data,
        ACL: 'public-read',
        ContentType: mimetype,
    }

    const imageUrl = s3.upload(uploadParams, function (err, data) {
        if (err) {
            console.log("Error", err);
        } if (data) {
            console.log("Upload Success", data.Location);
            // now insert data into json files
         
            let blog = {
                id: createID(5),
                title: title,
                short_Description: short_desription,
                description: description,
                photo: data.Location,
                content: content,
            }
            blogList.push(blog);
            fs.writeFile("routes/blog.json", JSON.stringify(blogList), err => {
                if (err) err;
                res.render("blogslist")
                console.log("Done writing"); // Success
            });
        }
    }); 
        
    console.log(imageUrl, "imageUrl");

})



router.post("/loginBlog", async function (req, res) {
    console.log(req.body, "bodddddddd");
   const {username, password} = req.body;
   console.log(username, password, "user naem pass");
   if (username === "admin@test.com" && password === "12345678") {
       console.log("ogged in");
    req.session.loggedin  = true;
    res.redirect("/createBlog")
   }
   else{
      return res.json({errorMessage: "The username or password is incorrect."});
   }
});


router.post("/logout", async function (req, res){
    req.session.loggedin = false;
    res.redirect("/createBlog")
})


router.get("/How-to-host-a-static-website-on-amazon-S3-through-stridefuture-AWS-portal", async function (req, res, next) {
    res.render("blogs/ddw");
});

router.get("/Deploying-static-website-on-aws-S3-by-manually-and-using-Stridefuture-aws", async function (req, res, next) {
    res.render("blogs/compare");
});

router.get("/How-to-deploy-static-website-manually-on-amazon-S3", async function (req, res, next) {
    res.render("blogs/manual");
});




router.get("/policy", async function (req, res, next) {
    res.render("policy");
});

// / to initialize the create bucekt section
router.get("/create", function (req, res, next) {
    res.render("create", { err: false });
});

// to post the data to the aws server to create the bucket
router.post("/create", createControllers.AddNewBucket);

// documents controlles
router.get("/document", function (req, res, next) {
    res.render("document", { title: "" });
}
);

// list create controllers

router.get("/list", listControllers.initListPage);
router.post("/list", listControllers.BucketsList);
router.post("/get-signed-url", listControllers.uploadSourceCode);

router.get('/thankyou', (req, res, next) => {
    res.render('thankyou')
})

// login controllers
router.get("/", loginControllers.login);
router.post("/", loginControllers.loginPost);


// to get the data from the server and render the template
// router.post("/uploadSource", upload, uploadControllers.UploadCode);
router.get("/upload", uploadControllers.codePage);


router.post("/createBlog", async function (req, res, next) {
    const { title, content, short_desription } = req.body;
    let blog = {
        id: createID(5),
        title: title,
        short_Description: short_desription,
        description: content,
        url: content,
        photo: content,
    }
    blogList.push(blog);
    fs.writeFile("routes/blog.json", JSON.stringify(blogList), err => {
        if (err) throw err;
        res.render("blog")
        console.log("Done writing"); // Success
    });

});
const createID = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}































module.exports = router;
