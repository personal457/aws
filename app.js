const http = require('http');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const path = require('path');
// app.use(bodyParser.json())

const fileUpload = require('express-fileupload');

app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))


const listRoutes = require('./routes/list');
const uploadRoutes = require('./routes/upload');
const createRoutes = require('./routes/create');
const loginRoutes = require('./routes/login');
const aboutRoutes = require('./routes/about')
const blogRoutes = require('./routes/blog')
const workflowRoutes = require('./routes/workflow')
const documentRoutes = require('./routes/document')

var session = require('express-session');


app.set('view engine', 'ejs'); 
app.set('views', 'views'); 
app.use(express.static(__dirname + '/'));
app.use(express.static(path.join(__dirname, 'assets')));
app.use(fileUpload());
app.use('/tinymce', express.static(path.join(__dirname, 'node_modules', 'tinymce')));
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));


app.use(listRoutes);
app.use(uploadRoutes);
app.use(createRoutes);
app.use(loginRoutes);
app.use(aboutRoutes);
app.use(blogRoutes);
app.use(workflowRoutes);
app.use(documentRoutes);

app.listen(process.env.PORT || 8080);
// const PORT = process.env.PORT || '8080';
const PORT = process.env.PORT;
// app.listen( PORT);